require "box2d"

b2.setScale(40)

-- bikin background
local bg = Bitmap.new(Texture.new("asset/background_lvl1.png"))
stage:addChild(bg)

-- this table holds the dynamic bodies and their sprites
local actors = {}

local world = b2.World.new(0,9.8)

-- bikin lantai
local ground = world:createBody({})
ground.name = "ground"
local shape = b2.EdgeShape.new(-200, 300, 680, 300)
ground:createFixture({shape = shape, density = 0})

-- bikin e114, di e114.lua
local e114 = world:createBody{type = b2.DYNAMIC_BODY, position = {x = 180,y=0}}
e114.name = "e114"
e114.offsetY = 0
e114.speed = 5
local e114shape = b2.PolygonShape.new()
e114shape:setAsBox(66,88)
e114:createFixture{shape = e114shape, density = 1, restitution = 0, friction = 0.3}
---- gambar e114, animated
local e114pack = TexturePack.new("asset/e114anim.txt", "asset/e114_kanan.png")
local e114bmptable = {
Bitmap.new(e114pack:getTextureRegion("e114_1.png")),
Bitmap.new(e114pack:getTextureRegion("e114_2.png")),
Bitmap.new(e114pack:getTextureRegion("e114_3.png")),
Bitmap.new(e114pack:getTextureRegion("e114_4.png"))
}
for key,bmp in pairs(e114bmptable) do
bmp:setAnchorPoint(0.5,0.5)
end
local e114mc = MovieClip.new{
{1,10,e114bmptable[1]},
{11,20,e114bmptable[2]},
{21,30,e114bmptable[3]},
{31,40,e114bmptable[4]}
}
e114mc:setGotoAction(40,1)
e114mc:gotoAndStop(1)
e114mc.playAnim = function()
e114mc:gotoAndPlay(1)
end
e114mc.stopAnim = function()
e114mc:gotoAndStop(1)
end
e114mc.stopAnim()
stage:addChild(e114mc)
actors[e114] = e114mc


-- bikin cage
local cage = world:createBody{type = b2.DYNAMIC_BODY, position = {x = 50,y=0}}
cage.name = "cage"
cage.offsetY = 20
local cageshape = b2.PolygonShape.new()
cageshape:setAsBox(86,107)
cage:createFixture{shape = cageshape, density = 1, restitution = 0, friction = 0.3}
----gamabr cage
local cagesprite = Bitmap.new(Texture.new("asset/labcage.png"))
cagesprite:setAnchorPoint(0.5,0.5)
stage:addChild(cagesprite)
actors[cage] = cagesprite


-- bikin tombol. limited animation w/ sensor.
local button = world:createBody{type = b2.STATIC_BODY, position = {x = 300, y = 300}}
button.name = "button"
cage.offsetY = 0
local buttonshape = b2.PolygonShape.new()

-- bikin sensor tombol. ukuran: selisih antara tombol saat dilepas
-- dengan tombol saat ditekan.


local function onEnterFrame()
	world:step(1/60, 8, 3)
	
	for body,sprite in pairs(actors) do
		local x,y = body:getPosition()
		y = y + body.offsetY
		sprite:setPosition(x,y)
		sprite:setRotation(body:getAngle() * 180 / math.pi)
	end
end

stage:addEventListener(Event.ENTER_FRAME, onEnterFrame)
