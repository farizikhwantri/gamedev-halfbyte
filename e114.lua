local e114 = self:createBody{type = b2.DYNAMIC_BODY, position = {x = 0,y=0}}
function e114:mulai(x,y)
 
e114.name = "e114"
e114.speed = 5
local e114shape = b2.PolygonShape.new()
e114shape:setAsBox(66,88)
e114:createFixture{shape = e114shape, density = 1, restitution = 0, friction = 0.3}
---- gambar e114, animated
local e114pack = TexturePack.new("e114anim.txt", "e114_kanan.png")
local e114bmptable = {
Bitmap.new(TexturePack:getTextureRegion("e114_1.png")),
Bitmap.new(TexturePack:getTextureRegion("e114_2.png")),
Bitmap.new(TexturePack:getTextureRegion("e114_3.png")),
Bitmap.new(TexturePack:getTextureRegion("e114_4.png"))
}
for bmp in e114bmptable do
bmp:setAnchorPoint(0.5,0.5)
end
local e114mc = MovieClip.new{
{1,10,e114bmptable[1]},
{11,20,e114bmptable[2]},
{21,30,e114bmptable[3]},
{31,40,e114bmptable[4]}
}
e114mc:setGotoAction(40,1)
e114mc:gotoAndStop(1)
e114mc.playAnim = function()
e114mc:gotoAndPlay(1)
end
e114mc.stopAnim = function()
e114mc:gotoAndStop(1)
end
e114mc.stopAnim()
end
return e114