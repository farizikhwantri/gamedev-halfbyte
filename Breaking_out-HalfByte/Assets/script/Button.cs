﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {

	public Sprite sp ;
	public Sprite idle ;
	public SpriteRenderer ren ;

	// Use this for initialization
	void Start () {
		ren = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D col){
		if(col.gameObject.tag.Equals("Player")){
			print("Player Push Button");
			ren.sprite = sp;
		}
		if(col.gameObject.tag.Equals("Lab Cage")){
			print("LabCage Hold Button");
			ren.sprite = sp;
		}
	}

	void OnCollisionStay2D(Collision2D col){
		if(col.gameObject.tag.Equals("Player")){
			print("Player Push Button");
			ren.sprite = sp;
		}
		if(col.gameObject.tag.Equals("Lab Cage")){
			print("LabCage Hold Button");
			ren.sprite = sp;
		}
	}

	void OnTriggerEnter2D(Collider2D col){
		if(col.gameObject.tag.Equals("Player")){
			print("Player Push Button");
			ren.sprite = sp;
		}
		if(col.collider2D.tag.Equals("Lab Cage")){
			print("LabCage Hold Button");
			ren.sprite = sp;
		}
	}

	void OnTriggerExit2D(Collider2D col){
		if(col.gameObject.tag.Equals("Player")){
			print("Player Push Button");
			ren.sprite = idle;
		}
		if(col.collider2D.tag.Equals("Lab Cage")){
			print("LabCage Hold Button");
			ren.sprite = idle;
		}
	}

}
