﻿using UnityEngine;
using System.Collections;

public class E114 : MonoBehaviour {
	[HideInInspector]
	public bool facingRight = true;			// For determining which way the player is currently facing.
	public GameObject Inventory ;
	public Vector3 releasePos ;
	public Animator anim ;

	private bool Hold ;

	// Use this for initialization
	void Start () {
		Input.simulateMouseWithTouches = true;
		anim = GetComponent<Animator>();
		Hold = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Fire2")){
			if(Hold){
				print ("Release Object");
				if(!facingRight){
					releasePos = new Vector3(transform.position.x - 1f , transform.position.y + 0.2f, transform.position.z);
				}
				else {
					releasePos = new Vector3(transform.position.x + 0.3f , transform.position.y + 0.2f, transform.position.z);
				}
				Release(releasePos);
			}
			else
				print("Empty Inventory");
		}

	}

	void FixedUpdate( ) {
		// Cache the horizontal input.
		float h = Input.GetAxis("Horizontal");
		int i = 0;
		if(Input.touchCount>i){
			h = Input.GetTouch(0).position.x - transform.position.x ;
			print(h.ToString());
		}
		else{
			if(Input.GetMouseButtonDown(0)){
				if(facingRight){

					float x = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;

					Vector2 distanceX = new Vector2(x, transform.position.y);

					anim.SetFloat("Speed", Mathf.Abs(x));

					rigidbody2D.AddForce(Vector2.right * x * 100f);

					print(x.ToString());

					if(x > 0 && !facingRight)
						// ... flip the player.
						Flip();
					// Otherwise if the input is moving the player left and the player is facing right...
					else if(x < 0 && facingRight)
						// ... flip the player.
						Flip();
				}
				else if(!facingRight){
					// 
					float x = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;

					Vector2 distanceX = new Vector2(x, transform.position.y);

					anim.SetFloat("Speed", Mathf.Abs(x));

					rigidbody2D.AddForce(Vector2.right * x * 100f);

					print(x.ToString());

					if(x > 0 && !facingRight)
						// ... flip the player.
						Flip();
					// Otherwise if the input is moving the player left and the player is facing right...
					else if(x < 0 && facingRight)
						// ... flip the player.
						Flip();
				}

			}
		}
		
		// The Speed animator parameter is set to the absolute value of the horizontal input.
		//anim.SetFloat("Speed", Mathf.Abs(h));
		
		// If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
		if(h * rigidbody2D.velocity.x < 2f){
			// ... add a force to the player.
			rigidbody2D.AddForce(Vector2.right * h * 15f);
		}
		
		// If the player's horizontal velocity is greater than the maxSpeed...
		if(Mathf.Abs(rigidbody2D.velocity.x) > 2f)
			// ... set the player's velocity to the maxSpeed in the x axis.
			rigidbody2D.velocity = new Vector2(Mathf.Sign(rigidbody2D.velocity.x) * 2f, rigidbody2D.velocity.y);

		if(h > 0 && !facingRight)
			// ... flip the player.
			Flip();
		// Otherwise if the input is moving the player left and the player is facing right...
		else if(h < 0 && facingRight)
			// ... flip the player.
			Flip();
	}

	void Flip ()
	{
		// Switch the way the player is labelled as facing.
		facingRight = !facingRight;
		
		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void OnCollisionEnter2D (Collision2D col){
		if(col.gameObject.tag.Equals("Lab Cage")){
			print("Interact");
		}
	}

	void HoldObject(GameObject things){
		//things.transform.localPosition.x 
	}

	void TakeCage(){
		print("Hold Cage");
		Hold = true;
		//Inventory = GameObject.FindGameObjectWithTag("Lab Cage");
	}

	void Release(Vector3 releasePos){
		print ("Release Inventory");
		Instantiate(Inventory, releasePos, transform.rotation);
		Hold = false;
	}
}
