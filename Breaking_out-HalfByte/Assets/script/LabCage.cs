﻿using UnityEngine;
using System.Collections;

public class LabCage : MonoBehaviour {

	public Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col){
		if(col.gameObject.tag.Equals("Player")){
			print("Player Near Cage");
		}
	}

	void OnTriggerStay2D(Collider2D col){
		if(col.gameObject.tag.Equals("Player")){
			if(Input.GetButtonDown("Fire1")){
				print("Player Take Cage 1");
				col.transform.SendMessage("TakeCage");
				Destroy(this.gameObject);
			}
		}
	}

	void OnCollisionStay2D(Collision2D col){
		if(col.gameObject.tag.Equals("Player")){
			if(Input.GetButtonDown("Fire1")){
				print("Player Take Cage 1");
				col.transform.SendMessage("TakeCage");
				Destroy(this.gameObject);
			}
		}
	}
}
